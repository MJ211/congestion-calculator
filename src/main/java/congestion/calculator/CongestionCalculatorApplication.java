package congestion.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CongestionCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CongestionCalculatorApplication.class, args);
	}
	//TODO: The application currently doesn't have an entry point, add a way to call the calculation with different inputs, preferrably over HTTP.

}
