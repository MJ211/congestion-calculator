package congestion.calculator;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CongestionTaxCalculator {
    //TODO: There is no particular structure to the code so there are several improvements that can be made.
    private static Map<String, Integer> tollFreeVehicles = new HashMap<>();

    //TODO: refactor vehicles
    static {
        tollFreeVehicles.put("Emergency", 2);
        tollFreeVehicles.put("Bus", 1);
        tollFreeVehicles.put("Diplomat", 3);
        tollFreeVehicles.put("Motorcycle", 0);
        tollFreeVehicles.put("Military", 5);
        tollFreeVehicles.put("Foreign", 4);
    }

    //TODO: The maximum amount per day and vehicle is 60 SEK.
    public int getTax(Vehicle vehicle, Date[] dates)
    {
        if(dates.length == 0) {
            return 0;
        }

        Date intervalStart = dates[0];
        //TODO BONUS: Just as you finished coding, your manager shows up and tells you that the same application should be used in other cities
        // with different tax rules. These tax rules need to be handled as content outside the application because different content editors for
        // different cities will be in charge of keeping the parameters up to date.
        // Move the parameters used by the application to an outside data store of your own choice to be read during runtime by the application.
        int totalFee = 0;

        for (int i = 0; i < dates.length ; i++) {
            Date date = dates[i];
            int nextFee = GetTollFee(date, vehicle);
            int tempFee = GetTollFee(intervalStart, vehicle);

            long diffInMillies = date.getTime() - intervalStart.getTime();
            long minutes = diffInMillies/1000/60;

            //TODO: A single charge rule applies in Gothenburg. Under this rule, a vehicle that passes several tolling stations within 60 minutes is
            // only taxed once. The amount that must be paid is the highest one.
            if (minutes <= 60)
            {
                if (totalFee > 0) totalFee -= tempFee; // Negative number?
                if (nextFee >= tempFee) tempFee = nextFee;
                totalFee += tempFee;
            }
            else
            {
                totalFee += nextFee;
            }
        }
      
        if (totalFee > 60) totalFee = 60;
        return totalFee;
    }

    private boolean IsTollFreeVehicle(Vehicle vehicle) {
        if (vehicle == null) return false;
        //TODO: what if null inside?
        String vehicleType = vehicle.getVehicleType();
        return tollFreeVehicles.containsKey(vehicleType);
    }

    //TODO: Congestion tax is charged during fixed hours for vehicles driving into and out of Gothenburg.
    //TODO: param null checks
    public int GetTollFee(Date date, Vehicle vehicle)
    {
        if (IsTollFreeDate(date) || IsTollFreeVehicle(vehicle)) return 0;

        int hour = date.getHours();
        int minute = date.getMinutes();

        //Bug: previously there were conditions like : (hour == 6 && minute >= 0 && minute <= 29). Different charge amount is set after 6:30, then
        // time 6:29 and 6:30 is not handled, but it should be involved by 6:29 congestion tax.

        if (hour == 6) {
            return minute < 30 ? 8 : 13;
        }
        else if (hour == 7) return 18;
        else if (hour == 8) {
            return minute < 30 ? 13 : 8;
        }

        //else if (hour >= 8 && hour <= 14 && minute >= 30 && minute <= 59) return 8; -> BUG it will get only minutes above or equal to 30
        else if (hour > 8 && hour < 15) return 8;

        else if (hour == 15) {
            return minute < 30 ? 13 : 18;
        }

        else if (hour == 16) return 18;

        else if (hour == 17) return 13;
        else if (hour == 18 && minute < 30) return 8;
        else return 0;
    }

    private Boolean IsTollFreeDate(Date date)
    {
        int year = date.getYear();
        int month = date.getMonth() + 1;
        int day = date.getDay() + 1;
        int dayOfMonth = date.getDate();

        if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) return true;

        //TODO: You may limit the scope to the year 2013.
        //TODO: The tax is not charged on weekends (Saturdays and Sundays), public holidays, days before a public holiday and during the month of
        // July.
        if (year == 2013)
        {
            if ((month == 1 && dayOfMonth == 1) ||
                    (month == 3 && (dayOfMonth == 28 || dayOfMonth == 29)) ||
                    (month == 4 && (dayOfMonth == 1 || dayOfMonth == 31)) || //bug: 30 instead of 31
                    (month == 5 && (dayOfMonth == 1 || dayOfMonth == 8 || dayOfMonth == 9)) ||
                    (month == 6 && (dayOfMonth == 5 || dayOfMonth == 6 || dayOfMonth == 21)) ||
                    (month == 7) ||
                    (month == 11 && dayOfMonth == 1) ||
                    (month == 12 && (dayOfMonth == 24 || dayOfMonth == 25 || dayOfMonth == 26 || dayOfMonth == 31)))
            {
                return true;
            }
        }
        return false;
    }
}
