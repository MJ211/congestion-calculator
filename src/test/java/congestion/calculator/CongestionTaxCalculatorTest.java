package congestion.calculator;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.Date;
import java.util.stream.Stream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

//TODO: As stated there may be bugs in the code, try to find and fix them.
class CongestionTaxCalculatorTest {
    private static final int NO_FEE = 0;
    private static final Vehicle TEST_VEHICLE = () -> "test vehicle";

    private final CongestionTaxCalculator underTest = new CongestionTaxCalculator();

    @Nested
    class GetTollFee {
        @Nested
        class ChargeNotFreeDaysAndTime {
            private static final Date TEST_CHARGING_DATE = new Date(2013, Calendar.FEBRUARY, 6, 6, 10);

            private static Stream<Vehicle> exemptVehicles() {
                return Stream.of(
                        () -> "Emergency",
                        () -> "Bus",
                        () -> "Diplomat",
                        () -> "Motorcycle",
                        () -> "Military",
                        () -> "Foreign");
            }

            @ParameterizedTest
            @MethodSource("exemptVehicles")
            void shouldFeeBeZeroForExemptVehiclesEvenIfInChargingHours(Vehicle vehicleType) {
                // when
                var result = underTest.GetTollFee(TEST_CHARGING_DATE, vehicleType);

                //then
                assertThat(result).isEqualTo(NO_FEE);
            }

            @Test
            void shouldFeeBeMoreThanZeroIfVehicleIsNull() {
                // when
                var result = underTest.GetTollFee(TEST_CHARGING_DATE, null);

                //then
                assertThat(result).isGreaterThan(NO_FEE);
            }

            @Test
            void shouldFeeBeMoreThanZeroIfVehicleNameIsNull() {
                // when
                var result = underTest.GetTollFee(TEST_CHARGING_DATE, () -> null);

                //then
                assertThat(result).isGreaterThan(NO_FEE);
            }
        }

        @Nested
        class ChargeFreeDaysAndTime {
            private static final Date TEST_NOT_CHARGING_DATE = new Date(2013, Calendar.FEBRUARY, 0, 0, 0);

            private static Stream<Vehicle> exampleVehicles() {
                return Stream.of(
                        () -> null,
                        null,
                        () -> "Emergency",
                        () -> "anythingElse");
            }

            @ParameterizedTest
            @MethodSource("exampleVehicles")
            void shouldFeeBeZeroForExemptVehiclesOutOfChargingHours(Vehicle vehicle) {
                // when
                var result = underTest.GetTollFee(TEST_NOT_CHARGING_DATE, vehicle);

                //then
                assertThat(result).isEqualTo(NO_FEE);
            }

            private static Stream<Date> chargeFreeDays() {
                var streamBuilder = Stream.<Date>builder();
                for (var i = 1; i <= 31; i++) {
                    streamBuilder.accept(atChargeTime(7, i));
                }
                streamBuilder.add(atChargeTime(1, 1));
                streamBuilder.add(atChargeTime(3, 28));
                streamBuilder.add(atChargeTime(3, 29));
                streamBuilder.add(atChargeTime(4, 1));
                streamBuilder.add(atChargeTime(4, 31));
                streamBuilder.add(atChargeTime(5, 1));
                streamBuilder.add(atChargeTime(5, 8));
                streamBuilder.add(atChargeTime(5, 9));
                streamBuilder.add(atChargeTime(6, 5));
                streamBuilder.add(atChargeTime(6, 6));
                streamBuilder.add(atChargeTime(6, 21));
                streamBuilder.add(atChargeTime(11, 1));
                streamBuilder.add(atChargeTime(12, 24));
                streamBuilder.add(atChargeTime(12, 25));
                streamBuilder.add(atChargeTime(12, 26));
                streamBuilder.add(atChargeTime(12, 31));
                return streamBuilder.build();
            }

            private static Date atChargeTime(int month, int day) {
                return new Date(2013, month - 1, day, 7, 30);
            }

            @ParameterizedTest
            @MethodSource("chargeFreeDays")
            void shouldFeeBeZeroForDaysFreeOfCharge(Date date) {
                // when
                var result = underTest.GetTollFee(date, TEST_VEHICLE);

                //then
                assertThat(result).isEqualTo(NO_FEE);
            }

            private static Stream<Arguments> timeBetweenSameFeeIsCharged() {
                return Stream.of(
                        Arguments.of(atChargeDate(0, 0, 0), atChargeDate(5, 59, 59), 0),
                        Arguments.of(atChargeDate(6, 0, 0), atChargeDate(6, 29, 59), 8),
                        Arguments.of(atChargeDate(6, 30, 0), atChargeDate(6, 59, 59), 13),
                        Arguments.of(atChargeDate(7, 0, 0), atChargeDate(7, 59, 59), 18),
                        Arguments.of(atChargeDate(8, 0, 0), atChargeDate(8, 29, 59), 13),
                        Arguments.of(atChargeDate(8, 30, 0), atChargeDate(14, 59, 59), 8),
                        Arguments.of(atChargeDate(15, 0, 0), atChargeDate(15, 29, 59), 13),
                        Arguments.of(atChargeDate(15, 30, 0), atChargeDate(16, 59, 59), 18),
                        Arguments.of(atChargeDate(17, 0, 0), atChargeDate(17, 59, 59), 13),
                        Arguments.of(atChargeDate(18, 0, 0), atChargeDate(18, 29, 59), 8),
                        Arguments.of(atChargeDate(18, 30, 0), atChargeDate(23, 59, 59), 0)
                );
            }

            private static Date atChargeDate(int hours, int minutes, int seconds) {
                return new Date(2013, Calendar.MARCH, 3, hours, minutes, seconds);
            }

            @ParameterizedTest
            @MethodSource("timeBetweenSameFeeIsCharged")
            void shouldFeeBeChargedAccordingToSchedule(Date specificFeeStart, Date specificFeeEnd, int expectedCharge) {
                // when
                var feeFromStart = underTest.GetTollFee(specificFeeStart, TEST_VEHICLE);
                var feeFromEnd = underTest.GetTollFee(specificFeeEnd, TEST_VEHICLE);

                //then
                assertThat(feeFromStart).isEqualTo(feeFromEnd).isEqualTo(expectedCharge);
            }
        }
    }
    @Nested
    class GetTaxUnitTests {
        @Test
        void shouldReturnZeroWhenNoDatesProvided() {
            // when
            var result = underTest.getTax(TEST_VEHICLE, new Date[]{});

            // then
            assertThat(result).isEqualTo(NO_FEE);
        }
    }



}
/*
Cases:
(getTax):

1 date
3 dates
dates longer than 60 minutes
dates shorter than 60 minutes

dates null
date inside dates null
vehicle null
vehicle name null



(GetTollFee):
no exempt The tax is not charged Saturdays and Sundays
exempt The tax is not charged Saturdays and Sundays
no exempt public holidays
exempt public holidays
no exempt days before a public holiday
exempt days before a public holiday
no exempt during the month of July.
exempt during the month of July.
etc.
 */