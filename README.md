# congestion-calculator
 
A coding task for a Volvo interview.


## Changelog

1. Prepare the project with CI set up
2. Add TODOs to code 
3. Write unit test for ensuring correctness of the code
    1. Test first method (GetTollFee) 
       1. Public holidays were determined based on: https://www.timeanddate.com/holidays/sweden/2013?hol=1
       2. Prepare cases
       3. Tests were added
       4. Several bugs were found and fixed (commit performed)
       5. Check Intellij test coverage to find out missing cases
       6. Added missing cases (commit performed)
    2. Test second method  (getTax)
       1. Prepare cases (commit performed)
       2. I was trying to understand how congestion tax calculation work in real life. I did not find any good resource explaining deeply how it 
works. For now I do not know how should I proceed.
       3. dasd




## Questions

1. (getTax) Should we act somehow when list of dates is empty?
2. (getTax) What dates should it process? From one day? From several? 
3. (general) Let's say a car passes several gates at hour: 6:15, 6:45, 7:10, 7:20, 15:10, 15:40. Taking into consideration "The single charge rule", 
is it needed to pay:  18 SEK (6:15, 6:45, 7:10) + 18 SEK (7:20) + 18 SEK (15:10, 15:40) = 54 SEK?